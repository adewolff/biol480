library("dplyr")
library("ggplot2")

data <- read.csv("data.csv", stringsAsFactors = F)

png("behaviour.png")
ggplot(data) + 
  geom_histogram(aes(x = behaviour), stat = "count") +
  theme_set(theme_classic(base_size = 18)) +
  theme(plot.title = element_text(size=17)) +
  labs(y = "instances",
       title = "Number of instances of unique behaviour")

dev.off()

individual <- data %>% group_by(individual) %>% summarise(behaviours = length(unique(behaviour)))
png("individuals.png")
ggplot(individual) + 
  geom_bar(aes(x = individual, y = behaviours), stat = "identity") +
  theme_set(theme_classic(base_size = 18)) +
  theme(plot.title = element_text(size=17)) +
  labs(y = "Number of unique behaviours",
       title = "Unique behaviours for each individual")

dev.off()


individual2 <- data %>% group_by(individual, behaviour) %>% summarise(instances = n())
individual2 <- individual2 %>% mutate(unique = paste(individual, behaviour))
png("individuals2.png")
ggplot(individual2) + 
  geom_histogram(aes(x = unique, y = instances), stat = "identity") +
  theme_set(theme_classic(base_size = 18)) +
  theme(plot.title = element_text(size=17),
        axis.text.x =element_text(angle = 50,
                                  vjust = 0.5)) +
  labs(y = "Occurences", x = "Individual and behaviour",
       title = "Occurences of unique behaviours for \n each individual")

dev.off()
