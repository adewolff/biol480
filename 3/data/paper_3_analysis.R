library("dplyr")
library("tidyr")
library("ggplot2")
library("leaflet")

data <- read.csv("fixed_coords.csv", stringsAsFactors = F)


# Map ---------------------------------------------------------------------

# Split coordinates into lat/long
data <- data %>%
  separate(coordinates, c("lat", "lng"), ",")

# Fix rounded coordinates for Margan East
data[data$location_name == "morgan_lake_east", 2] <- 46.900495
data[data$location_name == "morgan_lake_east", 3] <- -119.232263

# Convert coordinates to numeric
data$lat <- as.double(data$lat)
data$lng <- as.double(data$lng)

# Create map
m <- leaflet(data = data) %>% addTiles() %>%
  addMarkers(lat = data$lat, lng = data$lng, popup = data$location_name)


# Relation between direction and direction to water source ---------------------

numeric_orientation <- data %>% select(orientation, orientation_to_water)

# Turn values into numerical values
numerical <- function(column){
  unique_entries <- unique(column)
  n_unique <- length(unique_entries)
  
  for (i in c(1:n_unique)) {
    column[column == unique_entries[i]] <- i
  }
  
  return(as.numeric(column))
}

numeric_orientation$orientation <- numerical(numeric_orientation$orientation)
numeric_orientation$orientation_to_water <- numerical(
  numeric_orientation$orientation_to_water)

# Perform t-test
t_res <- t.test(numeric_orientation$orientation_to_water,
                numeric_orientation$orientation)

# T-test results
p_val <- as.numeric(t_res["p.value"])


# Preferred Direction -----------------------------------------------------

summary <- data %>%
  select(orientation) %>%
  group_by(orientation) %>%
  summarise(n = n())

pdf("../report/nest_orientations.pdf")
ggplot(summary, aes(x = orientation, y = n)) +
  geom_histogram(stat="identity") +
  geom_text(aes(label = n, y = n + mean(n / 10))) +
  theme_set(theme_classic(base_size = 18)) +
  theme(plot.title = element_text(size=17)) +
  labs(x = "nest orientation", y = "number of occurences",
       title = "Occurence of Nest Orientations in Colonies")
dev.off()
